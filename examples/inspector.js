/*
 * Use the evaluator as an inspector page, from a "legacy" script.
 * command: gjs inspector.js
 */

imports.gi.versions.Gtk = '4.0';
const Gtk = imports.gi.Gtk;

let application = new Gtk.Application();

application.connect('activate', () => {
    let window = new Gtk.Window({
        application,
        title: "Evaluator as an inspector page",
        defaultWidth: 300, defaultHeight: 150,
        child: new Gtk.Box({
            orientation: Gtk.Orientation.VERTICAL,
            halign: Gtk.Align.CENTER, valign: Gtk.Align.CENTER,
            spacing: 12,
        }),
    });

    let button = new Gtk.Button({ label: "Open Inspector" });
    button.connect('clicked', () => Gtk.Window.set_interactive_debugging(true));
    window.child.append(button);

    window.present();

    import('../evaluator/evaluator.js').then(({ default: Evaluator }) => {
        let evaluator = new Evaluator({
            context: {
                myBox: window.child,
            },
        });

        Evaluator.addToInspector(evaluator);
    }).catch(logError);
});

application.run([]);
