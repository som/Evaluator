/*
 * Use the evaluator as a window widget, from a "legacy" script.
 * command: gjs widget.js
 */

imports.gi.versions.Gtk = '4.0';
const Gtk = imports.gi.Gtk;

let application = new Gtk.Application();

application.connect('activate', () => {
    let window = new Gtk.Window({
        application, title: "Evaluator as a window widget",
        defaultWidth: 894, defaultHeight: 512,
    });

    import('../evaluator/evaluator.js').then(({ default: Evaluator }) => {
        window.child = new Evaluator({
            context: application,
        });
    }).catch(logError);

    window.present();
});

application.run([]);
