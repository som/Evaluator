/*
 * Copyright 2021 Abakkk
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-FileCopyrightText: 2021 Abakkk
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

import { Gio, GLib, GObject, Gtk } from './gi.js';

const Highlight = GObject.registerClass({
    GTypeName: 'EvaluatorHighlight',
}, class extends Gio.Subprocess {
    _init(dark = false) {
        let style = dark ? 'edit-vim-dark' : 'edit-kwrite';

        super._init({
            argv: ['highlight', '--fragment', '--out-format=pango', `--style=${style}`, '--syntax=js'],
            flags: Gio.SubprocessFlags.STDIN_PIPE | Gio.SubprocessFlags.STDOUT_PIPE | Gio.SubprocessFlags.STDERR_PIPE,
        });

        try {
            this.init(null);
        } catch(e) {
            throw new Error("For syntax highlighting, install the “highlight” program");
        }
    }

    getMarkupAsync(text, cancellable) {
        return new Promise((resolve, reject) => {
            this.communicate_utf8_async(text, cancellable, (source_, result) => {
                try {
                    let [sucess_, stdout, stderr] = this.communicate_utf8_finish(result);

                    if (this.get_exit_status() != 0) {
                        throw new Gio.IOErrorEnum({
                            code: Gio.io_error_from_errno(this.get_exit_status()),
                            message: stderr ? stderr.trim() : GLib.strerror(this.get_exit_status())
                        });
                    }

                    // Remove the global font attributes.
                    let markup = stdout.slice(stdout.indexOf('>') + 1, stdout.lastIndexOf('<') - 1);

                    // Highlight remove the ending new line, which is problematic for an editor.
                    if (text[text.length - 1] == '\n')
                        markup += '\n';

                    resolve(markup);
                } catch(e) {
                    reject(e);
                }
            });
        });
    }
});

export const InputBuffer = GObject.registerClass({
    GTypeName: 'EvaluatorInputBuffer',
    Properties: {
        'as-function': GObject.ParamSpec.boolean(
            'as-function', "As function", "Whether the code must be evaluated as a function",
            GObject.ParamFlags.READWRITE, false
        ),
        'indentation': GObject.ParamSpec.uint(
            'indentation', "Indentation", "The number of characters for the indentation",
            GObject.ParamFlags.READWRITE, 1, 32, 4
        ),
        'syntax-highlighting': GObject.ParamSpec.boolean(
            'syntax-highlighting', "Syntax highlighting", "Whether syntax highlighting is enabled",
            GObject.ParamFlags.READWRITE, !!GLib.find_program_in_path('highlight')
        ),
        'syntax-highlighting-available': GObject.ParamSpec.boolean(
            'syntax-highlighting-available', "Syntax highlighting available",
            "Whether syntax highlighting is available (if false, overwrite 'syntax-highlighting')",
            GObject.ParamFlags.READWRITE, true
        ),
        'syntax-highlighting-dark': GObject.ParamSpec.boolean(
            'syntax-highlighting-dark', "Syntax highlighting dark", "Whether the syntax highlighting theme is dark",
            GObject.ParamFlags.READWRITE, false
        ),
        'use-tabs': GObject.ParamSpec.boolean(
            'use-tabs', 'Use tabs', 'Whether the indentation is done with Tab characters',
            GObject.ParamFlags.READWRITE, false
        ),
    },
    Signals: {
        'backward': {},
        'forward': {},
        'up': { param_types: [GObject.TYPE_INT] },
        'down': { param_types: [GObject.TYPE_INT] },
        'evaluate': { param_types: [GObject.TYPE_STRING] },
        'complete': { param_types: [GObject.TYPE_STRING] },
    },
}, class extends Gtk.TextBuffer {
    _init(params) {
        super._init(params);

        this._updateSyntaxHighlighting();
    }

    _indent() {
        let [hasSelection_, startIter, endIter] = this.get_selection_bounds();
        let startLine = startIter.get_line();
        let endLine = endIter.get_line();

        for (let line = startLine; line <= endLine; line++) {
            let startIter = this.get_iter_at_line(line)[1];
            let text = new Array(this.indentation).fill(this.useTabs ? '\t' : ' ').join('');
            this.insert(startIter, text, -1);
        }
    }

    _unindent() {
        let [hasSelection_, startIter, endIter] = this.get_selection_bounds();
        let startLine = startIter.get_line();
        let endLine = endIter.get_line();

        for (let line = startLine; line <= endLine; line++) {
            let startIter = this.get_iter_at_line(line)[1];
            let iter = startIter.copy();

            let i = 0;
            while (!iter.get_char().trim() && i < this.indentation) {
                iter.forward_char();
                i++;
            }

            this.delete(startIter, iter);
        }
    }

    _updateSyntaxHighlighting() {
        if (this.syntaxHighlighting) {
            this._changeHandler = this.connect('changed', () => {
                if (!this.syntaxHighlightingAvailable)
                    return;

                try {
                    this._highlightCancellable?.cancel();
                    this._highlightCancellable = new Gio.Cancellable();

                    new Highlight(this.syntaxHighlightingDark).getMarkupAsync(this.text, this._highlightCancellable)
                        .then(markup => {
                            this.block_signal_handler(this._changeHandler);
                            let cursorPosition = this.cursorPosition;
                            this.text = '';
                            this.insert_markup(this.get_end_iter(), markup, -1);
                            this.place_cursor(this.get_iter_at_offset(cursorPosition));
                            this.unblock_signal_handler(this._changeHandler);
                        }).catch(e => {
                            if (e.code != Gio.IOErrorEnum.CANCELLED)
                                log(e.message);
                        });
                } catch(e) {
                    log(e.message);
                    this.syntaxHighlightingAvailable = false;
                }
            });
            this.emit('changed');
        } else if (this._changeHandler) {
            this.disconnect(this._changeHandler);
            this.set_text(this.text, -1);
        }
    }

    handleUp() {
        if (!this.get_char_count()) {
            this.emit('up', -1);
            return true;
        }

        let matches = this.getText().match(/^r\((\d+)\)$/);
        if (matches) {
            this.emit('up', parseInt(matches[1]));
            return true;
        }

        return false;
    }

    handleDown() {
        let matches = this.getText().match(/^r\((\d+)\)$/);
        if (matches) {
            this.emit('down', parseInt(matches[1]));
            return true;
        }

        return false;
    }

    handleTab(shift) {
        if (shift) {
            this._unindent();
            return;
        }

        if (!this.get_has_selection()) {
            let endIter = this.get_iter_at_mark(this.get_insert());
            if (endIter.backward_char() && endIter.get_char().trim()) {
                endIter.forward_char();
                let startIter = this.get_start_iter();
                let text = startIter.get_text(endIter);
                this.emit('complete', text);
                return;
            }
        }

        this._indent();
    }

    activate() {
        let text = this.getText();
        if (text.trim())
            this.emit('evaluate', text);

        this.setText('');
    }

    get syntaxHighlighting() {
        return this._syntaxHighlighting ??
            this.constructor[GObject.properties]['syntax-highlighting'].get_default_value();
    }

    set syntaxHighlighting(syntaxHighlighting) {
        if (this.syntaxHighlighting == syntaxHighlighting)
            return;

        this._syntaxHighlighting = syntaxHighlighting;
        this.notify('syntax-highlighting');

        this._updateSyntaxHighlighting();
    }

    getText() {
        return this.get_text(this.get_start_iter(), this.get_end_iter(), false);
    }

    setText(text) {
        this.set_text(text, -1);
    }

    setResult(index) {
        this.setText(`r(${index})`);
    }
});

export const OutputBuffer = GObject.registerClass({
    GTypeName: 'EvaluatorOutputBuffer',
    Signals: {
        'text-added': {},
        'result-activated': { param_types: [GObject.TYPE_UINT] },
        'invalidate-gutter-marks': {},
    },
}, class extends Gtk.TextBuffer {
    _addGutterMark(text) {
        let mark = this.create_mark(null, this.get_end_iter(), true);
        this.gutterMarks.push(Object.assign(mark, { text }));
    }

    _appendText(text, ...tagNames) {
        let startOffset = this.get_end_iter().get_offset();
        this.insert(this.get_end_iter(), text, -1);

        tagNames.forEach(tagName => {
            this.apply_tag(this.tagTable.lookup(tagName), this.get_iter_at_offset(startOffset), this.get_end_iter());
        });
    }

    _appendTags(...tagNames) {
        tagNames.forEach(tagName => {
            let endIter = this.get_end_iter();
            let startIter = this.get_iter_at_line(endIter.get_line())[1];
            this.apply_tag(this.tagTable.lookup(tagName), startIter, endIter);
        });
    }

    _createSuggestion() {
        this.create_mark('suggestion', this.get_end_iter(), true);
    }

    deleteSuggestion() {
        let mark = this.get_mark('suggestion');
        if (!mark)
            return;

        this.delete(this.get_iter_at_mark(mark), this.get_end_iter());
        this.delete_mark(mark);

        this.gutterMarks.pop();
        this.emit('invalidate-gutter-marks');
    }

    get gutterMarks() {
        return (this._gutterMarks ?? (this._gutterMarks = []));
    }

    pushCode(text) {
        this.deleteSuggestion();

        if (this.get_char_count()) {
            this._appendTags('below');
            this._appendText('\n');
        }

        this._addGutterMark(">>>");
        this._appendText(text || " ");
    }

    pushResult(text, tagName, isClickable, index) {
        this.deleteSuggestion();

        this._appendText('\n');

        let tagNames = [tagName];
        if (isClickable) {
            this.tagTable.add(new Gtk.TextTag({ name: String(index) }));
            tagNames.push(String(index));
        }

        this._addGutterMark(`r(${index})`);
        this._appendText(text || " ", ...tagNames);
        this.emit('text-added');
    }

    pushSuggestion(text) {
        this.deleteSuggestion();
        this._createSuggestion();

        this._appendTags('below');
        this._appendText('\n');
        this._addGutterMark("?");
        this._appendText(text);
        this.emit('text-added');
    }

    getLastLineMark() {
        if (!this.get_mark('last-line'))
            this.create_mark('last-line', this.get_iter_at_line(this.get_line_count() - 1)[1], false);

        this.move_mark_by_name('last-line', this.get_iter_at_line(this.get_line_count() - 1)[1]);
        return this.get_mark('last-line');
    }

    handleMotion(iter) {
        this._resultIndex = iter.get_tags().map(tag => parseInt(tag.name)).find(int => !isNaN(int));
        return typeof this._resultIndex == 'number';
    }

    handleClick() {
        this.emit('result-activated', this._resultIndex);
    }
});
